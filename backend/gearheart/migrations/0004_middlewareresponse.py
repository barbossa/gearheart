# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gearheart', '0003_product_color'),
    ]

    operations = [
        migrations.CreateModel(
            name='MiddlewareResponse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('method', models.CharField(max_length=10)),
                ('status_code', models.SmallIntegerField()),
                ('path', models.CharField(max_length=250)),
            ],
        ),
    ]
