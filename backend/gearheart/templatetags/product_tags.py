from django import template
from gearheart.models import Product

register = template.Library()


@register.inclusion_tag('templatetags/random_product.html')
def random_product():
    products = Product.objects.order_by('?')[:5]
    return {'products': products}

