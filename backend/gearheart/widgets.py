from django.forms import TextInput


class ColorPicker(TextInput):
    class Media:
        css = {
            'all': ('colorpicker/css/colorpicker.css',)
        }
        js = ('colorpicker/js/bootstrap-colorpicker.js', 'custom_admin.js')

    def render(self, name, value, attrs=None):
        attrs = dict(attrs)
        attrs.update({'class': 'colorpicker'})
        return super(TextInput, self).render(name, value, attrs=attrs)

