def get_user_data(user):
    data = {
        'user_id': user.id,
        'username': user.username,
        'email': user.email,
        'is_superuser': user.is_superuser,
    }
    return data
