from django.contrib import admin
from gearheart.models import Product, MiddlewareResponse
from gearheart.forms import ColorForm


class ProductAdmin(admin.ModelAdmin):
    list_display = ['title', 'description', 'price', 'published']
    form = ColorForm


admin.site.register(Product, ProductAdmin)


class MiddlewareResponseAdmin(admin.ModelAdmin):
    list_display = ['method', 'status_code', 'path']


admin.site.register(MiddlewareResponse, MiddlewareResponseAdmin)
