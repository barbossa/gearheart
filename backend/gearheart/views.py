from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from gearheart.helpers import get_user_data
from rest_framework import generics
from gearheart.models import Product, MiddlewareResponse
from gearheart.serializers import ProductSerializer
from gearheart.permissions import IsAdminOrReadOnly
from django.shortcuts import render


class LoginView(ObtainAuthToken):
    def post(self, request):
        serializer = AuthTokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key, 'user': get_user_data(user)})


class ProductList(generics.ListCreateAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = (IsAdminOrReadOnly,)

    def get_queryset(self):
        queryset = Product.objects.all()
        if not self.request.user.is_staff:
            queryset = queryset.filter(published=True)
        return queryset


class ProductDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAdminOrReadOnly,)
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


def system_page(request):
    responses = MiddlewareResponse.objects.all()
    return render(request, 'system.html', {'responses': responses})
