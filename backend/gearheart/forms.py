from django import forms
from gearheart.models import Product
from gearheart.widgets import ColorPicker

class ColorForm(forms.ModelForm):

    class Meta:
        model = Product
        widgets = {
            'color': ColorPicker(),
        }
        fields = "__all__"
