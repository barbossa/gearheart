from django.db import IntegrityError
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from .models import Product


class UserTests(APITestCase):
    def test_login(self):
        """
        User must have a valid token after authentication. Response status should be 200
        """
        user = User.objects.create_user(username='obiwan', email='obiwan@gmail.com', password='1')
        data = {'username': 'obiwan', 'password': 1}
        url = '/api/login/'
        response = self.client.post(url, data, format='json')
        token = Token.objects.get(user=user)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['token'], token.key)


class ProductTests(APITestCase):
    def test_get_admin_product(self):
        """
        Admin can see all products.
        """
        client = APIClient()
        User.objects.create_superuser(username='admin', email='admin@gmail.com', password='1')
        token = Token.objects.get(user__username='admin')
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        url = '/api/products/'
        products = Product.objects.bulk_create([
            Product(title='product-1', price=1, published=True),
            Product(title='product-2', price=4, published=False),
            Product(title='product-3', price=2, published=True),
            Product(title='product-4', price=9, published=True),
            Product(title='product-5', price=3, published=False),
        ])
        response = client.get(url)
        self.assertEqual(response.data['count'], len(products))

    def test_get_user_product(self):
        """
        Regular user can see only published products.
        """
        User.objects.create_user(username='obiwan', email='obiwan@gmail.com', password='1')
        client = APIClient()
        token = Token.objects.get(user__username='obiwan')
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        url = '/api/products/'
        Product.objects.bulk_create([
            Product(title='product-1', price=1, published=True),
            Product(title='product-2', price=4, published=False),
            Product(title='product-3', price=2, published=True),
            Product(title='product-4', price=9, published=True),
            Product(title='product-5', price=3, published=False),
        ])
        response = client.get(url)
        self.assertEqual(response.data['count'], 3)

    def test_product_title_field_must_be_unique(self):
        """
        Product title must be unique.
        """
        Product.objects.create(title='product-1', price=1, published=True)
        with self.assertRaises(IntegrityError):
            Product.objects.create(title='product-1', price=6, published=False)

    def test_product_price_field_must_be_required(self):
        """
        Product price must be required.
        """
        with self.assertRaises(IntegrityError):
            Product.objects.create(title='product-1', published=False)

    def test_regular_user_cannot_edit_product(self):
        """
        Regular user cannot edit product. Only admin can
        """
        User.objects.create_user(username='obiwan', email='obiwan@gmail.com', password='1')
        Product.objects.create(title='product-1', price=1, published=True)
        client = APIClient()
        token = Token.objects.get(user__username='obiwan')
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        url = '/api/products/1'
        data = {'title': 'product-edited'}
        response = client.put(url, data, format='json')
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.data['detail'], 'You do not have permission to perform this action.')
