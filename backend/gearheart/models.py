from django.db import models
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User
from django.core.mail import send_mail


class Product(models.Model):
    title = models.CharField(max_length=250, unique=True)
    description = models.TextField(blank=True, null=True)
    price = models.IntegerField()
    published = models.BooleanField(default=False)
    color = models.CharField(max_length=10, blank=True, null=True)

    def __unicode__(self):
        return self.title


@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    """Automatically generate user token"""
    if created:
        Token.objects.create(user=instance)


def notify_admins(action, instance):
    users = User.objects.filter(is_superuser=True)

    for user in users:
        subject = 'The product was {0}'.format(action)
        message = 'Hi, {0}. The product {1} was {2}'.format(user.username, instance.title.encode('utf-8'), action)
        send_mail(subject, message, 'noreplay@gearheart.io', [user.email], fail_silently=False)


def product_post_save(sender, instance, created, **kwargs):
    """Notify all admins when product was added/edited"""

    action = 'edited'
    if created is True:
        action = 'created'

    notify_admins(action, instance)


def product_post_delete(sender, instance, **kwargs):
    """Notify all admins when product was deleted"""
    notify_admins('deleted', instance)


post_save.connect(product_post_save, sender=Product)
post_delete.connect(product_post_delete, sender=Product)


class MiddlewareResponse(models.Model):
    method = models.CharField(max_length=10)
    status_code = models.SmallIntegerField()
    path = models.CharField(max_length=250)
