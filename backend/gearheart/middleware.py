from gearheart.models import MiddlewareResponse


class AllResponsesMiddleware(object):
    def process_response(self, request, response):
        MiddlewareResponse.objects.create(method=request.method, status_code=response.status_code, path=request.path)
        return response
