from django.conf.urls import include, url
from django.contrib import admin
from gearheart.views import LoginView, ProductList, ProductDetail, system_page

urlpatterns = [
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^api/login/$', LoginView.as_view()),
    url(r'^api/products/$', ProductList.as_view()),
    url(r'^api/products/(?P<pk>[0-9]+)$', ProductDetail.as_view()),
    url(r'^system/$', system_page, name='system_page'),

]
