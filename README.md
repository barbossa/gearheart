# Установка #
Если необходимо, установите недостающие пакеты 
```
#!bash
sudo apt-get install python3 libpq-dev python-dev postgresql postgresql-contrib
```
## Разворачивание проекта ##
**Backend**
```
#!bash
git clone git@bitbucket.org:barbossa/gearheart.git
cd gearheart/backend
virtualenv .env
source .env/bin/activate
pip install -r requirements.txt
python manage.py migrate
python manage.py loaddata gearheart.json
python manage.py runserver
```

**Frontend**
```
#!bash
cd ../frontend
curl -sL https://deb.nodesource.com/setup | sudo bash -
sudo apt-get install -y nodejs build-essential
sudo npm install -g grunt-cli bower
npm install
bower install
npm install -g coffee-script
coffee --compile --output static/js/app app/
node server.js
```

После этого приложение будет доступно по адресу http://localhost:2015/

Если по каким-то причинам приложение недоступно по адресу http://localhost:2015/, настраиваем nginx.
```
#!bash
sudo touch /etc/nginx/sites-available/gearheart.ls
sudo ln -s /etc/nginx/sites-available/gearheart.ls /etc/nginx/sites-enabled/
sudo nano /etc/nginx/sites-available/gearheart.ls
```
Далее вставляем в /etc/nginx/sites-available/gearheart.ls этот конфиг
```
#!bash
server {
    server_name gearheart.ls www.gearheart.ls;

    access_log   /home/pc-8/projects/gearheart/frontend/logs/access.log;
    error_log    /home/pc-8/projects/gearheart/frontend/logs/error.log;

    root /home/pc-8/projects/gearheart/frontend;
    index index.html;
    charset utf-8;

    location / {
        try_files $uri $uri/ /index.html =404;
  }
}
```
Пути к проекту в конфиге необходимо заменить на свои

Добавляем в /etc/hosts новую запись
```
#!bash
127.0.1.1       gearheart.ls
```
Перезапускаем nginx
```
#!bash
sudo service nginx restart
```
Приложение будет доступно по адресу http://gearheart.ls/#/

Admin user: admin/admin

Regular user: obiwan/1