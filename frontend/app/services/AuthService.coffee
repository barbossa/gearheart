angular
.module('gearheartApp')
.factory 'LoginService', [
  '$http'
  '$window'
  '$q'
  'API_SERVER'
  ($http, $window, $q, API_SERVER) ->
    authenticate = (username, password, endpoint) ->
      url = API_SERVER + endpoint
      data = undefined
      deferred = $q.defer()
      data =
        'username': username
        'password': password
      $http.post(url, data).then ((response) ->
        token = response.data.token
        user = response.data.user
        if token and username
          $window.localStorage.setItem('token', token);
          $window.localStorage.setItem('user', JSON.stringify(user));
          deferred.resolve true
        else
          deferred.reject 'Invalid data received from server'
        return
      ), (response) ->
        deferred.reject response.data.error
        return
      deferred.promise

    logout = ->
      deferred = $q.defer()
      url = API_SERVER + 'logout/'
      $http.post(url).then (->
        $window.localStorage.removeItem 'token'
        $window.localStorage.removeItem 'username'
        deferred.resolve()
        return
      ), (error) ->
        deferred.reject error.data.error
        return
      deferred.promise

    {
    login: (username, password) ->
      authenticate username, password, 'login/'
    logout: ->
      logout()

    }
]

.factory 'UserService', [
  '$window'
  '$state'
  ($window, $state) ->
    service =
      init: ->
        service.postActions()
        return
      postActions: ->
        console.log 'post actions'
        return
      isLogged: ->
        token = $window.localStorage.getItem 'token'
        if token
          return true
        else
          return false
      currentUser: ->
        user = $window.localStorage.getItem 'user'
        if user
          return JSON.parse $window.localStorage.getItem 'user'
        else
          return false
      logout: ->
        $window.localStorage.removeItem 'token'
        $window.localStorage.removeItem 'user'
        $state.go 'login'
        return
      isAdmin: ->
        service.currentUser().is_superuser
    service
]
