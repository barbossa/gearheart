angular.module('gearheartApp')
.factory 'ProductRestService', [
  '$resource'
  'API_SERVER'
  ($resource, API_SERVER) ->
    $resource API_SERVER + 'products/:id?format=json', {id: '@_id'},
      isArray: false
      update:
        method: 'PUT'
]