'use strict'

angular.module('gearheartApp', [
  'ui.router'
  'ngResource'
  'ngSanitize'
  'permission'
])

angular.module('gearheartApp').run [
  'Permission'
  'UserService'
  (Permission, UserService) ->
    Permission
    .defineRole 'admin', (stateParams) ->
      if UserService.isAdmin()
        return true
      false
    .defineRole 'logged', (stateParams) ->
      if UserService.isLogged()
        return true
      false
    return
]

angular.module('gearheartApp').config [
  '$stateProvider'
  '$httpProvider'
  '$urlRouterProvider'
  ($stateProvider, $httpProvider, $urlRouterProvider) ->
    $httpProvider.interceptors.push 'AuthInterceptor'
    $urlRouterProvider.when '', '/products'
    $urlRouterProvider.otherwise '/products'
    $stateProvider
    .state 'dashboard',
      url: '/'
      abstract: true
      templateUrl: 'templates/pages/dashboard/index.html'

    .state 'dashboard.products',
      url: 'products'
      templateUrl: 'templates/pages/dashboard/products.html'
      controller: 'ProductsController'
      data: permissions:
        only: ['admin', 'logged']
        redirectTo: 'login'

    .state 'dashboard.products.view',
      url: "/:id/view"
      templateUrl: 'templates/pages/dashboard/view-product.html'
      controller: 'ProductsViewController'
      data: permissions:
        only: ['admin', 'logged']
        redirectTo: 'login'

    .state 'dashboard.products.edit',
      url: "/:id/edit"
      templateUrl: 'templates/pages/dashboard/edit-product.html'
      controller: 'ProductsEditController'
      data: permissions:
        only: ['admin']
        redirectTo: 'login'

    .state 'login',
      url: '/login'
      templateUrl: 'templates/pages/login.html'
      controller: 'LoginController'
    return
]

angular.module('gearheartApp').constant 'API_SERVER', 'http://127.0.0.1:8000/api/'