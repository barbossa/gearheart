angular.module('gearheartApp')
.directive 'colorPicker', ->
  {
    restrict: 'A'
    require: 'ngModel'
    link: (scope, element, attrs, ctrl) ->
      $(element).colorpicker scope.$eval(attrs.colorPicker)
      $(element).colorpicker().on 'changeColor', (ev) ->
        ctrl.$setViewValue(ev.color.toHex())
        ctrl.$render();
        return
      return

  }