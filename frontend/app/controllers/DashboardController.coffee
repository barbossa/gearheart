'use strict'

angular.module('gearheartApp')
.controller 'ProductsController', [
  '$scope'
  'ProductRestService'
  ($scope, ProductRestService) ->
    $scope.products = ProductRestService.get();
    return
]

.controller 'ProductsViewController', [
  '$scope'
  'ProductRestService'
  '$stateParams'
  ($scope, ProductRestService, $stateParams) ->
    $scope.loadProduct = ->
      ProductRestService.get {id: $stateParams.id}, (product) ->
        $scope.product = product
        return
      return

    $scope.loadProduct()

    return
]

.controller 'ProductsEditController', [
  '$scope'
  'ProductRestService'
  '$stateParams'
  ($scope, ProductRestService, $stateParams) ->
    $scope.errors = ''
    $scope.success = ''
    $scope.loadProduct = ->
      ProductRestService.get {id: $stateParams.id}, (product) ->
        $scope.product = product
        return
      return

    $scope.loadProduct()

    $scope.updateProduct = ->
      $scope.product.$update {id:$scope.product.id}, (->
        $scope.success = 'Product have been successfully saved'
        return
      ), (httpResponse) ->
        $scope.errors = httpResponse.data
        return
      return

    return
]