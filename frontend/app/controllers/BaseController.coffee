'use strict'

angular
.module('gearheartApp')
.controller 'MasterController', [
  '$scope'
  'UserService'
  ($scope, UserService) ->
    $scope.$watch UserService.isLogged, (isLogged) ->
      $scope.isLogged = isLogged
      $scope.currentUser = UserService.currentUser()
      return

    $scope.logout = ->
      UserService.logout()
    return
]

.controller 'LoginController', [
  '$scope'
  '$location'
  'LoginService'
  '$state'
  ($scope, $location, LoginService, $state) ->
    # little hack. if user is logged redirect him to dashboard
    if $scope.isLogged
      $state.go 'dashboard.products'
    $scope.login = ->
      username = $scope.loginUsername
      password = $scope.loginPassword
      if username and password
        LoginService.login(username, password).then (->
          $state.go 'dashboard.products'
          return
        ), (error) ->
          $scope.loginError = error
          return
      else
        $scope.loginError = 'Username and password required'
      return

    return
]
