angular.module('gearheartApp').factory 'AuthInterceptor', [
  '$q'
  '$window'
  '$injector'
  ($q, $window, $injector) ->
    {
      request: (config) ->
        config.headers = config.headers or {}
        if $window.localStorage.token
          config.headers.Authorization = 'Token ' + $window.localStorage.token
        config
      responseError: (response) ->
        switch response.status
          when 401
            userService = $injector.get 'UserService'
            userService.logout()
        $q.reject response

    }
]